
var BenoitBot = BenoitBot || {};

/*
 * Amazon S3 config
 */
BenoitBot.S3 = {
    bucket: 'https://benoitdessine.s3-eu-west-1.amazonaws.com/',
    folders: {
        animations: 'bot/animations/',
        images:     'bot/drawings/'
    }
};
