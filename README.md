
# BenoîtBot-CC: scripts pour Adobe Creative Suite (After Effects, Illustrator)

## 1. Pré-requis

* Mac OS avec la voix système "Thomas" (installée par défaut, accessible dans *Préférences Système > Dictée et parole*)

* Synchronisation des fichiers Creative Cloud dans le dossier `~/Creative Cloud Files/`


## 2. Téléchargement et installation

* **[Télécharger le code](https://bitbucket.org/ylakim/benoitbot-cc/get/HEAD.zip)**

* **Décompresser** l'archive ZIP et renommer le dossier dézippé en `BenoitBot-CC`

* Copier le dossier BenoitBot-CC dans le **dossier "Scripts" d'After Effects** (`/Applications/Adobe After Effects <version>/Scripts/`)

* Copier le fichier `BenoitBot.jsx` dans le **dossier "ScriptUI Panels"** (`/Applications/Adobe After Effects <version>/Scripts/ScriptUI Panels/`)

* Relancer After Effects (si déjà ouvert)


## 3. Configuration d'After Effects

* Ouvrir les préférences d'After Effects

* Dans le panneau *Général*, sélectionner l'option `Autoriser les scripts à écrire des fichiers et à accéder au réseau` (en anglais "Allow Scripts to Write Files and Access Network")

* L'interface du script est activable via le menu `Fenêtre > BenoitBot.jsx`. Le panneau peut être ancré dans l'interface d'After Effects.


## 4. Configuration d'Illustrator

* Copier le fichier `BenoitBot - Publier Dessin.jsx` dans le **dossier "Scripts"** d'Illustrator (`/Applications/Adobe Illustrator <version>/Presets/fr_FR/Scripts/`)

* Relancer Illustrator (si déjà ouvert)

* Le script pour publier une image sur le BenoîtBot est accessible via le menu `Fichier > Scripts`

