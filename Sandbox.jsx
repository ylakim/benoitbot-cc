﻿/*
 * Sandbox
 */

//@target "aftereffects"

//@include "Helpers.jsx"

//@include "lib/json2.js"
//@include "lib/http-client/lib/http-client.full.jsx"
//@include "lib/adobe-javascript-http-client/http.jsx"

/* global app */

(function() {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         _currentScript.path,
            templates: {
                prefixLayer: '>'
            }
        };
        
    clearOutput();
    app.beginUndoGroup( _config.scriptName );
        
    function testHTTP() {

        var _url;
        _url = 'umaps.fr';
        _url = 'adobe.com';

        var _response = $http({
            method: 'GET',
            //url: 'http://date.jsontest.com/'
            url: 'http://benoitdessine.com/'
        });

        createFile( _config.folder + '/exports/http.log', JSON.stringify( _response.payload ) );

//        var _client = new HttpClient( _url, {
//            encoding: 'utf8',
//            timeout: 10,
//            headers: {
//                "User-Agent": "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
//            }
//        });
//
//        _client.get( '/', function(response) {
//            writeLn( "StatusCode:    " + response.statusCode );
//            createFile( _config.folder + '/exports/http.log', response.body );
//        });

    }
    
    function testCompAndLayers() {
        
        try {
            var $comp = app.project.activeItem,
                $layer = $comp.selectedLayers[0];
            
            
            $layer.timeRemapEnabled = false;
            $layer.timeRemapEnabled = true;
            
            var _timeLastFrame = $layer.outPoint - $comp.frameDuration,
                _timeRemap = $layer.property('Time Remap');
            
            _timeRemap.addKey( _timeLastFrame );
            _timeRemap.removeKey( _timeRemap.numKeys );
            
            _timeRemap.setInterpolationTypeAtKey( _timeRemap.numKeys, KeyframeInterpolationType.LINEAR, KeyframeInterpolationType.HOLD );
            
            $layer.outPoint  = $comp.duration;
            
        } catch( e ) {
            alert( logException( e ) );
        }

        
    }
    
    function testCreateAudio() {
        
        var _projectName        = 'Sant\u00E9',
            _projectFolderFS    = '/Users/Mikaly/Desktop/Tests AE/' + _projectName,
            _date               = currentDateForFile(),
            _subtitles          = [ 'Vous souffrez de crampes nocturnes aux mollets ?' ],
            i = 1;
        //debug( File.encode( "Santé" ) );
        var _suffix         = '_' + ( i+1 ),
            _textFS         = _projectFolderFS + '/Texte' + _suffix + '.txt',
            _textFile       = createFile( _textFS, _subtitles.join(' ') ),
            _soundFS        = _projectFolderFS + '/Voix-off_auto' + _suffix + '.aiff',
            _voice          = 'Thomas', // default French voice on Mac OS X
            _cmd = Mustache.render(
                'cd {{{directory}}}; say -v {{{voice}}} -r {{{speech_rate}}} -o "{{{sound_file}}}" -f "{{{text_file}}}"', {
                voice:          _voice,
                speech_rate:    200,
                directory:      cmdFile( _textFile.parent.fsName ),
                sound_file:     'test.aiff',
                text_file:      cmdFile2( _textFile.name )
            }),
            _resCmd         = system.callSystem( _cmd );
        debug( _cmd );
        debug( _resCmd );
    }
    
    
    try {
        
        //testHTTP();        
        testCompAndLayers();
        //testCreateAudio();
        

        //alert( 'Sandbox: done.');

    } catch( e ) {
        writeLn('Une erreur est survenue.');
        alert( e, _config.scriptName );
    }        

    app.endUndoGroup();

})();
