﻿/*
 * Film Production
 */

//@target "aftereffects"

//@include "Helpers.jsx"
//@include "Config.jsx"

/* global Mustache */

/* global app */
/* global system */
/* global Folder */
/* global File */
/* global Checkbox */
/* global CompItem */
/* global AVLayer */
/* global TextLayer */
/* global FootageItem */
/* global SolidSource */
/* global FileSource */
/* global RQItemStatus */
/* global CloseOptions */
/* global KeyframeInterpolationType */
/* global photoshop */

var BenoitBot = BenoitBot || {};

BenoitBot.Film_Production = (function( _self ) {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            plugin:         'BenoitBot',
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         Folder.decode( _currentScript.path + '/' ),
            outputFolder:   Folder.decode( _currentScript.path + '/output/' ),
            templates: {
                prefixLayer: '>'
            }
        };
    
    /**
     * Generate film (AE project and compositions) from JSON timeline
     * @param {Object} options
     * @returns {Boolean} success
     */
    _self.generateFilm = function( options ) {
        
        var _options = extend({
            json_str: '',
            json: {
                meta: {
                    name: ''
                },
                timeline: []
            },
            voice_over: false,
            projectFolder: app.settings.haveSetting( _config.plugin, 'projectFolder' ) ? app.settings.getSetting( _config.plugin, 'projectFolder' ) : '~/Desktop'
        }, options );
        
        try {
            
            if( app.project === null ) {
                alert("Créez d'abord un projet avant de continuer.");
                return false;
            }
            
            app.beginUndoGroup( _config.scriptName );
            
            /*
             * Import templates project
             */
            var _json_str           = ( _options.json_str || '' ).trim(),
                _json               = extend( _options.json, JSON.parse( _json_str ) ),
                _templateFile       = "~/Creative Cloud Files/+ ANIMATIONS template/Templates_Animation_Benoitdessine_Style_Ooreka_V4.aep",
                _templateProject    = File( _templateFile ),
                _projectName        = ( _json.meta.name || 'Film' ).toString().removeDiacritics(),
                _date               = currentDateForFile(),
                _projectFolder      = Folder( _options.projectFolder + '/' + _projectName + ' (' + _date + ')' ),
                _projectFolderFS    = Folder.decode( _projectFolder.fsName );
            
            if( !_projectFolder.create() ) {
                alert( "Le dossier du film n'a pu être créé (" + _projectFolderFS + ').' );
                return false;
            }
            
            var _newProject         = File( _projectFolderFS + '/' + _projectName + ' (' + _date + ').aep' );
            
            app.open( _templateProject );
            var _saved = app.project.save( _newProject );
            if( !_saved ) {
                alert( "L'import ne peut continuer car le projet n'a pas été sauvegardé." );
                app.project.close( CloseOptions.DO_NOT_SAVE_CHANGES );
                return false;
            }

            var _timelineFolder = app.project.items.addFolder("Timeline"),
                _precompFolder  = app.project.items.addFolder("Precomps"),
                _imagesFolder   = app.project.items.addFolder("Images"),
                _audioFolder    = app.project.items.addFolder("Audio"),
                _compSubsOpts   = { name: 'Sous-titrage' },
                $compSubs       = findComp( app.project, _compSubsOpts ),
                $layerSub       = $compSubs instanceof CompItem ? $compSubs.layer(1) : false,
                $compAnnotation = findComp( app.project, { name: 'Animation neutre' } ),
                $layerAnnotation = $compAnnotation instanceof CompItem ? $compAnnotation.layer(1) : false,
                _tempFiles      = [];
            
            if( $layerSub === false ) {
                var _errorMsg = Mustache.render( 'La composition de sous-titres est introuvable ({{{info}}})', { info: _compSubsOpts.name } );
                writeLn( _errorMsg );
                debug( _errorMsg );
            }
            
            var $compGlobal = app.project.items.addComp( 'Timeline / Montage', 1920, 1080, 1, 10, 25 );
            $compGlobal.bgColor = [ 1, 1, 1 ];
            $compGlobal.openInViewer();
            var _currentTime = 0;
            
            /*
             * Create compositions
             */
            for( var i = 0; i < _json.timeline.length; i++ ) {
                
                var _comp       = _json.timeline[i],
                    $tplComp    = findComp( app.project, { id: _comp.comp_id } );

                if( !( $tplComp instanceof CompItem ) ) {
                    continue;
                }
                
                var $comp = $tplComp.duplicate();
                $comp.parentFolder = _timelineFolder;
                $comp.name = '[' + (i+1) + '] ' + $tplComp.name;
                
                /**
                 * Update layers from a comp
                 * @param {CompItem} $comp
                 * @param {Object} compData
                 * @param {CompItem} $parentComp
                 * @param {integer} depth
                 */
                function updateCompLayers( $comp, compData, $parentComp, depth ) {
                    
                    for( var j = 0; j < compData.layers.length; j++ ) {
                        
                        var _layer      = compData.layers[j],
                            $layer      = findLayer( $comp, { placeholder: _layer.name } );
                    
                        if( $layer === false ) {
                            debug( 'Layer not found (placeholder = ' + _layer.name + ') in "' + $comp.name + '" comp.' );
                            continue;
                        }
                    
                        var _sourceAV   = $layer.source, // AVItem: FootageItem or CompItem
                            _isText     = $layer instanceof TextLayer,
                            _isFootage  = _sourceAV instanceof FootageItem,
                            _isImage    = _isFootage && _sourceAV.mainSource instanceof FileSource,
                            _isBox      = isBoxLayer( $layer );

                        $layer.locked = false;
                        
                        /*
                         * Text
                         */
                        if( _isText ) {
                            $layer.property( "sourceText" ).setValue( _layer.text );
                        }

                        /*
                         * Image
                         */
                        if( _layer.type === 'image' ) {
                            
                            var $newImage;
                            
                            // Image => Image
                            if( _isImage ) {
                                var _importFile = File( _layer.image.file );
                                if( _importFile.exists ) {
                                    $newImage = app.project.importFile( new ImportOptions( _importFile ) );
                                    $layer.replaceSource( $newImage, true );
                                    $newImage.parentFolder = _imagesFolder;
                                }
                            
                            // Image => Box
                            } else if( _isBox ) {
                                
                                var _AI_fs          = _layer.image.file,
                                    _AI_file        = File( _AI_fs ),
                                    _EPS_file       = File( getEPSfile( _AI_fs ) ),
                                    _importFile     = _EPS_file.exists ? _EPS_file : _AI_file;
                                
                                if( _importFile.exists ) {
                                    $newImage = app.project.importFile( new ImportOptions( _importFile ) );
                                    replaceLayerInBox( $layer, $newImage );
                                    $newImage.parentFolder = _imagesFolder;
                                    
                                } else {
                                    
                                }
                                
                            }
                            
                        }

                        /*
                         * Precomp
                         */
                        if( _layer.type === 'precomp' ) {

                            var $tplPreComp = findComp( app.project, { id: _layer.precomp.comp_id } );
                            if( !( $tplPreComp instanceof CompItem ) ) {
                                continue;
                            }

                            var $preComp = $tplPreComp.duplicate();
                            $preComp.parentFolder = _precompFolder;
                            
                            depth++;
                            $preComp.name = '[' + (i+1) + '.' + depth + '] ' + _layer.name;

                            if( _isBox ) {
                                replaceLayerInBox( $layer, $preComp );
                            } else {
                                $layer.replaceSource( $preComp, true );
                            }
                            
                            $layer.collapseTransformation = false;
                            
                            updateCompLayers( $preComp, _layer.precomp, $comp, depth );
                        }
                    }
                    
                    createTexts( $comp, compData, $parentComp, depth );
                    cleanCompLayers( $comp );
                }
                
                /**
                 * Clean a comp
                 * @param {CompItem} $comp
                 */
                function cleanCompLayers( $comp ) {                    
                    for( var j = 1; j <= $comp.layers.length; j++ ) {
                        var $layer              = $comp.layers[j],
                            _hasSpecialName     = $layer.name.search('--') === 0,
                            _isEmptyBox         = isBoxLayer( $layer, false ),
                            _clean              = _hasSpecialName || _isEmptyBox;
                        if( _clean ) {
                            $layer.locked = false;
                            $layer.remove();
                            j--;
                        }
                    }
                }
                
                function getEPSfile( _AI_filepath ) {
                    var _parts = _AI_filepath.replace(/\.ai$/, '.eps').split('/');
                    if( !_parts.length ) {
                        return _AI_filepath;
                    }
                    var _last = _parts.pop();
                    _parts.push( '- EPS' );
                    _parts.push( _last );
                    return _parts.join('/');
                }
                
                function replaceLayerInBox( $layer, $newSource ) {
                    
                    var _box = {
                            width:          $layer.width,
                            height:         $layer.height,
                            scale:          $layer.scale.value
                        },
                        _space = {
                            width:  _box.scale[0]/100 * _box.width,
                            height: _box.scale[1]/100 * _box.height
                        },
                        _padding = {
                            width:  0.20,
                            height: 0.20
                        };
                    
                    _space.width = _space.width * ( 1 - _padding.width );
                    _space.height = _space.height * ( 1 - _padding.height );
                    
                    $layer.replaceSource( $newSource, true );
                    
                    var _isHorizontal = ( $layer.width > $layer.height ),
                        _ratios = {
                            layer:  $layer.width / $layer.height,
                            space:  _space.width / _space.height
                        };

                    var _newScale = 100 * ( _ratios.layer > _ratios.space ? _space.width / $layer.width : _space.height / $layer.height );
                    
                    $layer.scale.setValue([ _newScale, _newScale, 100 ]);
                    
                    var _inPoint = $layer.inPoint;
                    $layer.startTime    = _inPoint;
                    $layer.inPoint      = _inPoint;
                }
                
                updateCompLayers( $comp, _comp, null, 0 );
                
                /**
                 * Create voice-over, subtitles and annotation
                 * @param {CompItem} $comp
                 * @param {Object} compData
                 * @param {integer} depth
                 */
                function createTexts( $comp, compData, $parentComp, depth ) {
                    
                    var _text           = (compData.text || '').trim(),
                        _subtitles      = _text.split("\n"),
                        _subDuration    = 1.5;

                    /* 
                     * Text-to-speech
                     */
                    var $voiceLayer = null;
                    if( _options.voice_over && _text ) {
                        var _prefix         = toPermalink( _projectName ) + '__' + _date + '__',
                            _suffix         = '_' + (i+1) + '.' + depth,
                            _textFS         = _projectFolderFS + '/Texte' + _suffix + '.txt',
                            _textFile       = createFile( _textFS, _subtitles.join(' ') ),
                            _soundFS        = _projectFolderFS + '/Voix-off_auto' + _suffix + '.aiff',
                            _lang           = _json.meta.lang || 'fr',
                            _voices = {
                                'fr' : 'Thomas', // default French voice on Mac OS X
                                'en' : 'Samantha',
                            },
                            _voice          = _voices[ _lang ] || _voices['fr'],
                            _cmd = Mustache.render(  
                                'say -v {{{voice}}} -r {{{speech_rate}}} -o {{{sound_file}}} -f {{{text_file}}}', {
                                voice:          _voice,
                                speech_rate:    170, // words per minute
                                sound_file:     cmdFile( _soundFS ),
                                text_file:      cmdFile( _textFS )
                            }),
                            _resCmd         = system.callSystem( _cmd );
                        if( !_resCmd ) {
                            //debug( _cmd );
                            var _soundFile = File( _soundFS );
                            if( _soundFile.exists ) {
                                var _voiceFile = app.project.importFile( new ImportOptions( _soundFile ) );
                                _voiceFile.parentFolder = _audioFolder;
                                _subDuration = _voiceFile.duration / _subtitles.length;
                                $voiceLayer = $comp.layers.add( _voiceFile );
                            }
                            _tempFiles.push( _textFile );
                        } else {
                            debug( _cmd );
                            debug( _resCmd );
                        }
                    }

                    /*
                     * Subtitles
                     */
                    if( $layerSub !== false ) {
                        for( var j = 0; j < _subtitles.length; j++ ) {

                            var _subtitle = _subtitles[j].trim();

                            if( !_subtitle ) {
                                continue;
                            }

                            var $subtitle = copyLayerToComp( $layerSub, $comp );

                            $subtitle.sourceText.setValue( _subtitle );
                            $subtitle.name = '';

                            $subtitle.startTime = 0;
                            $subtitle.inPoint   = ( j ) * _subDuration;
                            $subtitle.outPoint  = ( j + 1 ) * _subDuration;
                            
                            if( $parentComp instanceof CompItem ) {
                                var $subtitleParentComp = copyLayerToComp( $subtitle, $parentComp ),
                                    $compLayerInParent  = findLayer( $parentComp, { comp_id: $comp.id });
                                if( $compLayerInParent !== false ) {
                                    $subtitleParentComp.startTime = $compLayerInParent.inPoint;
                                } else {
                                    debug( 'Layer not found (comp #' + $comp.id + ' / "' + $comp.name + '") in "' + $parentComp.name + '" comp (for subtitle)' );
                                }
                                $subtitle.enabled = false;
                            }
                        }
                    }

                    if( $voiceLayer instanceof AVLayer ) {
                        $voiceLayer.moveToBeginning();
                        if( $voiceLayer.outPoint > $comp.duration ) {
                            $comp.duration          = $voiceLayer.outPoint;
                            $comp.workAreaStart     = 0;
                            $comp.workAreaDuration  = $comp.duration;
                        }
                    }

                    /*
                     * Modify the timing of a layout comp
                     */
                    if( compData.is_layout ) {
                        var $boxLayers = [];
                        for( var j = 1; j <= $comp.layers.length; j++ ) {
                            var $layer = $comp.layers[j],
                                _isBox = isBoxLayer( $layer, true );
                            if( _isBox ) {
                                $boxLayers.push( $layer );
                            }
                        }
                        if( $boxLayers.length > 0 ) {
                            $boxLayers.sort( sortNamesAlphabeticallySpecial );
                            var _boxDuration = $comp.duration / $boxLayers.length;
                            for( var j = 0; j < $boxLayers.length; j++ ) {

                                // Time for box layers
                                var $layer = $boxLayers[j];
                                $layer.startTime = ( j ) * _boxDuration;
                                $layer.inPoint   = ( j ) * _boxDuration;
                                $layer.outPoint  = $comp.duration;

                                // Time remapping (if needed)
                                if( $layer.canSetTimeRemapEnabled && $layer.outPoint !== $comp.duration ) {
                                    $layer.timeRemapEnabled = true;

                                    var _timeLastFrame  = $layer.outPoint - $comp.frameDuration,
                                        _timeRemap      = $layer.property('Time Remap');

                                    _timeRemap.addKey( _timeLastFrame );
                                    _timeRemap.removeKey( _timeRemap.numKeys );

                                    _timeRemap.setInterpolationTypeAtKey( _timeRemap.numKeys, KeyframeInterpolationType.LINEAR, KeyframeInterpolationType.HOLD );

                                    $layer.outPoint = $comp.duration;
                                }
                            }
                        }
                    }

                    /*
                     * Add annotation for animator
                     */
                    if( $layerAnnotation !== false && compData.annotation ) {
                        var $newLayerAnnotation = copyLayerToComp( $layerAnnotation, $comp );
                        $newLayerAnnotation.sourceText.setValue( compData.annotation );
                        $newLayerAnnotation.startTime = 0;
                        $newLayerAnnotation.inPoint   = 0;
                        $newLayerAnnotation.outPoint  = $comp.duration;
                    }
                }
                

                /*
                 * Add comp to global timeline
                 */
                var $layerCompInGlobal = $compGlobal.layers.add( $comp, $comp.duration );
                $layerCompInGlobal.name = i+1;
                $layerCompInGlobal.startTime = _currentTime;
                _currentTime += $comp.duration;
            
            } // End of timeline loop
            
            // Adjust global timeline
            if( $layerCompInGlobal instanceof AVLayer && $layerCompInGlobal.outPoint > $compGlobal.duration ) {
                $compGlobal.duration = $layerCompInGlobal.outPoint;
            }
            
            /*
             * Cleaning
             */
            app.project.reduceProject( [ _timelineFolder, $compGlobal ] );
            app.project.save();
            for( var i = 0; i < _tempFiles.length; i++ ) {
                var _tempFile = _tempFiles[i];
                if( _tempFile instanceof File ) {
                    _tempFile.remove();
                }
            }
            
            app.endUndoGroup();
            return true;
            
        } catch( e ) {
            logFile( 'generate_film', logException( e ) );
            //writeLn( 'Erreur: ' + e );
            alert( "Une erreur est survenue : \n" + logException( e ) );
            app.endUndoGroup();
            return false;
        }
        
    };

    
    return _self;
    
})( BenoitBot.Film_Production || {} );