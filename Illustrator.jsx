﻿/*
 * Convert AI files to PNG with Illustrator
 */

//@target "illustrator"

//@include "Helpers.jsx"
//@include "Config.jsx"

/* global Mustache */
/* global AI_Shell */

/* global app */
/* global Window */
/* global Folder */
/* global File */
/* global Checkbox */
/* global SaveOptions */
/* global ExportType */
/* global XMPConst */
/* global UserInteractionLevel */

var BenoitBot = BenoitBot || {};

BenoitBot.Illustrator = (function( _self ) {
    
    var _currentScript = (new File( $.fileName )),
        _config = {
            plugin:         'BenoitBot',
            scriptName:     _currentScript.name.replace(/\.jsx$/, ''),
            folder:         Folder.decode( _currentScript.path + '/' ),
            outputFolder:   Folder.decode( _currentScript.path + '/output/' ),
            logsFolder:     Folder.decode( _currentScript.path + '/logs/' )
        },
        _settings = {
            //projectFolder: app.settings.haveSetting( _config.plugin, 'projectFolder' ) ? app.settings.getSetting( _config.plugin, 'projectFolder' ) : '~/Desktop'
        };
    
    /**
     * This function builds the user interface.
     * @returns {Window|Panel} Window or Panel object representing the built user interface.
     */
    function buildUI() {

        var $palette = new Window( 'dialog' /*'palette'*/, _config.scriptName, undefined, { resizeable: true } );

        if( $palette === null ) {
            return $palette;
        }
        
        function updateUILayout( $container ) {
            $container.layout.layout( true ); //Update the container  
            $palette.layout.layout( true ); //Then update the main UI layout  
        }
        
        $palette.margins = 5;
        $palette.spacing = 5;
        $palette.alignment = 'fill';
        //$palette.maximumSize.width = 100;
        $palette.minimumSize.width = 300;
        
        $palette.logo = $palette.add( "image", undefined, File( _config.folder + 'images/logo_orange_small.png' ) );
        //$palette.logo.alignment = 'left';
        
        var $tabs = $palette.add( "tabbedpanel", undefined );
        $tabs.margins = 0;
        $tabs.spacing = 0;
        $tabs.alignment = 'fill';
        
        
        
        /* ----------------
         *    Export tab
         * ---------------- */

        var $exportTab = $tabs.add( "tab", undefined, 'Export' );
        $exportTab.margins = 15;
        $exportTab.spacing = 15;
        $exportTab.alignment = 'fill';
        //$exportTab.add( "scrollbar", undefined );

        var $exportImages = $exportTab.add( "panel", undefined, 'Export de plusieurs images' );
        $exportImages.margins = 15;
        $exportImages.alignment = 'fill';
        $exportImages.alignChildren = "left";
        
        $exportImages.export_png        = $exportImages.add( "checkbox", undefined, 'Export en PNG' );
        $exportImages.export_eps        = $exportImages.add( "checkbox", undefined, 'Export en EPS' );
        $exportImages.upload            = $exportImages.add( "checkbox", undefined, 'Upload sur BenoîtBot' );
        //$exportImages.algolia_records   = $exportImages.add( "checkbox", undefined, 'Export pour Algolia' );
        
        var _exportDefaults = exportDefaults();
        for( var _option in _exportDefaults ) {
            if( $exportImages[ _option ] instanceof Checkbox ) {
                $exportImages[ _option ].value = _exportDefaults[_option];
            }
        }

        var $btnSeveralImages = $exportImages.add( "button", undefined, 'Exporter' );
        $btnSeveralImages.alignment = 'center';
        $btnSeveralImages.onClick = function() {
            $btnSeveralImages.enabled = false;
            var _options = {
                    $logMsg: $logMsg,
                    $palette: $palette
                };
            for( var _option in _exportDefaults ) {
                if( $exportImages[ _option ] instanceof Checkbox ) {
                    _options[ _option ] = $exportImages[ _option ].value;
                }
            }
            exportImages( _options );
            $btnSeveralImages.enabled = true;
        };
        
//        var $btnCurrentImage = $exportImages.add( "button", undefined, 'Exporter image courante' );
//        $btnCurrentImage.onClick = function() {
//        };

        var $logMsg = $exportTab.add( "statictext", undefined, "", { multiline: true } );
        $logMsg.alignment = 'fill';
        $logMsg.preferredSize.height = 60;


        var $btnClose = $exportTab.add( "button", undefined, 'Fermer' );
        $btnClose.alignment = 'right';
        $btnClose.onClick = function() {
            $palette.hide();
        };

        
        /*
         * Finishing UI
         */
        $palette.layout.layout(true);
        $palette.layout.resize();
        $palette.onResizing = $palette.onResize = function() {
            this.layout.resize();
        };

        return $palette;
    }
    
    /**
     * Set the PNG saving options of the files using the PDFSaveOptions object.
     */
    function getPNGOptions() {
        
        var pngExportOpts = new ExportOptionsPNG24();
        
        pngExportOpts.antiAliasing = true;
        pngExportOpts.artBoardClipping = true;
        //pngExportOpts.matte = true;
        //pngExportOpts.matteColor = 0, 0, 0;
        pngExportOpts.saveAsHTML = false;
        pngExportOpts.transparency = true;
        
        pngExportOpts.horizontalScale   = 25.0; // equivalent to 480×270 pixels
        pngExportOpts.verticalScale     = 25.0;
        
        return pngExportOpts;
    }


    function exportDefaults() {
        return {
            batch:              true,
            export_png:         true,
            export_eps:         true,
            upload:             true,
            validate_xmp:       false, // validate XMP metadata before uploading PNG
            algolia_records:    true
        };
    }
    
    /**
     * Export images (bulk)
     * @param {Object} options
     */
    function exportImages( options ) {
        
        options = extend( exportDefaults(), options );
        var _batch             = options.batch,
            _useActiveDocument = !_batch;
        
        // cf https://forums.adobe.com/message/4510805
        
        try {
            
            app.userInteractionLevel = UserInteractionLevel.DONTDISPLAYALERTS; // Suppress Illustrator warning dialogs
            
            if( _useActiveDocument && !app.documents.length ) {
                alert('Veuillez ouvrir un fichier Illustrator.');
                return;
            }
            
            // Source files
            var _sourceFiles = _useActiveDocument ? [ app.activeDocument.fullName ] : File.openDialog( 'Sélectionnez les fichiers AI à convertir en PNG', undefined, true );
            if( !_sourceFiles || !_sourceFiles.length ) {
                alert( 'Aucun fichier trouvé.', _config.scriptName );
                return;
            }
            
            // Destination to save the files
            var _defaultFolder = _sourceFiles[0].parent,
                _pngFolder = Folder( _defaultFolder.fsName + '/- PNG' ),
                _epsFolder = Folder( _defaultFolder.fsName + '/- EPS' );
                //_pngFolder = Folder.selectDialog( 'Sélectionnez le dossier dans lequel vous souhaitez enregistrer les fichiers PNG convertis.', _defaultFolder );
            
            loadXMPLibrary();
            
            /* --------------------
             *    Export process
             * -------------------- */
            
            var _numFiles = _sourceFiles.length,
                _algolia_records = [],
                _timeStart = (new Date()).getTime();
            
            for( var i = 0; i < _numFiles; i++ ) {
                
                var _sourceFile = _sourceFiles[i],
                    _isAiFile   = _sourceFile.name.match(/\.ai$/),
                    _name       = File.decode( _sourceFile.name.replace(/\.ai$/, '') );
                
                if( !_isAiFile ) {
                    continue;
                }
                
                /*
                 * XMP metadata
                 */
                var _XMP_file   = new XMPFile( _sourceFile.fsName, XMPConst.UNKNOWN, XMPConst.OPEN_FOR_READ ),
                    _XMP        = _XMP_file.getXMP();
                _XMP_file.closeFile();

                var _descriptionXMP = _XMP.getArrayItem( XMPConst.NS_DC, "description", 1 ),
                    _description    = _descriptionXMP.toString().replace('\r','\n'),
                    _countKeywords  = _XMP.countArrayItems( XMPConst.NS_DC, "subject" ),
                    _keywords       = [];
                
                for( var j = 1; j <= _countKeywords; j++ ) {
                    _keywords.push( _XMP.getArrayItem( XMPConst.NS_DC, "subject", j ).toString() );
                }
                
                if( _useActiveDocument && options.validate_xmp ) {
                    var _XMP_errors = [];
                    if( !_description.trim().length ) {
                        _XMP_errors.push( "Cette image n'a pas de description." );
                    }
                    if( !_keywords.length ) {
                        _XMP_errors.push( "Cette image n'a pas de mots-clés." );
                    }
                    if( _XMP_errors.length > 0 ) {
                        var _confirmMsg = Mustache.render( 
                                "Les informations de l'image sont incomplètes.\n" +
                                "- {{{list_errors}}}\n\n" + 
                                "Cliquez sur \"Oui\" pour uploader quand même l'image sur le BenoîtBot.",
                                { list_errors: _XMP_errors.join('\n- ') }
                            ),
                            _confirmXMPvalidation = confirm( _confirmMsg, true, 'test' );
                        if( !_confirmXMPvalidation ) {
                            return;
                        }
                    }
                }                
                
                /*
                 * Open file
                 */
                var _openFile = ( options.export_png || options.export_eps );
                if( _openFile ) {    
                    var _sourceDoc = _useActiveDocument ? app.activeDocument : app.open( _sourceFile );
                }
                
                /*
                 * Export to PNG
                 */
                var _pngFilename    = toPermalink( _name ) + '.png',
                    _pngFullPath    = _pngFolder + '/' + _pngFilename;
                if( options.export_png ) {
                    var _targetFile = new File( _pngFullPath );
                    _sourceDoc.exportFile( _targetFile, ExportType.PNG24, getPNGOptions() );
                }

                /*
                 * Export to EPS
                 */
                if( options.export_eps ) {
                    var _epsFilename    = File.encode( _name ) + '.eps', // prevent a bug with special characters like %
                        _epsFullPath    = _epsFolder + '/' + _epsFilename,
                        _epsFile        = new File( _epsFullPath ),
                        _epsOptions     = new EPSSaveOptions();
                    _epsOptions.compatibility = Compatibility.ILLUSTRATOR8;
                    _sourceDoc.saveAs( _epsFile, _epsOptions );
                }
                
                /*
                 * Close file
                 */
                if( _openFile && _batch || _useActiveDocument && options.export_eps ) {
                    _sourceDoc.close( SaveOptions.DONOTSAVECHANGES );
                }
                
                var _sourceFSname = _sourceFile.toString();
                
                /*
                 * Algolia export
                 */
                var _tag        = _sourceFSname.toLowerCase().search('ooreka') !== -1 ? 'Ooreka' : 'Général',
                    _tagFolder  = toPermalink( _tag ) + '/',
                    _record = {
                        name:           _name,
                        file:           File.decode( _sourceFSname ),
                        description:    _description,
                        keywords:       _keywords,
                        tag:            _tag,
                        created_at:     _sourceFile.created,
                        modified_at:    _sourceFile.modified,
                        saved_at:       (new Date()).toString(),
                        size:           _sourceFile.length,
                        schema_version: '2016-12-07'
                    };

                _record.objectID = '[' + _tag + ']_' + _name;

                _record.png_s3_key  = BenoitBot.S3.folders.images + _tagFolder + _pngFilename;
                _record.png_url     = BenoitBot.S3.bucket + _record.png_s3_key;
                
                _algolia_records.push( _record );
                
                /*
                 * Upload to BenoîtBot-DB
                 */
                if( options.upload ) {
                    var _isProd     = true,
                        _domain     = _isProd ? 'http://bot.benoitdessine.com' : 'http://benoitbot.local',
                        _uploadUrl  = _domain + '/images/save',
                        _cmd = Mustache.render(
                            'cd {{{png_folder}}}; curl -X POST -F "{{{png_input}}}=@{{{png_filename}}};type=image/png" -F record={{{record}}} {{{url}}}', {
                            png_folder:     cmdFile( _pngFolder.fsName ),
                            png_input:      /*cmdFile*/ ( 'image_trait:upload:preview_image' ),
                            png_filename:   cmdFile( _pngFilename ),
                            //source_file:    cmdFile( _sourceFSname ),
                            //description:    cmdFile( _description ),
                            //keywords:       cmdFile( _keywords ),
                            record:         cmdFile( JSON.stringify( _record ) ),
                            url:            _uploadUrl
                        }),
                        _debugResCmd = false;
                    debug( _cmd, 'Upload to DB' );
                    
                    AI_Shell.launchCmd( _cmd, function( responseUpload ) {
                        
                        debug( responseUpload, 'Retour upload' );
                        var _json = typeof responseUpload === 'string' ? JSON.parse( responseUpload ) : responseUpload;
                        
                        if( _json.success ) {
                            
                        // Error
                        } else {
                            if( _useActiveDocument ) {
                                if( _json.timeout ) {
                                    _json.error = 'La requête HTTP semble anormalement longue.';
                                }
                                var _errorMsg = Mustache.render( 
                                    "Une erreur est survenue lors de l'envoi de l'image. \n{{{error}}}", 
                                    { error: _json.error || '' }
                                );
                                alert( _errorMsg );
                            }
                        }
                        
                    }, _debugResCmd );
                }
                
                /*
                 * Log
                 */
                if( options.$logMsg && options.$palette ) {
                    var _count = i+1,
                        _countLeft = _numFiles - _count,
                        _duration = ((new Date()).getTime() - _timeStart )/1000,
                        _timeLeft = _duration/_count * _countLeft;
                    options.$logMsg.text = Mustache.render('{{count}} {{items}} sur {{total}} ({{percent}}%)\nTemps restant : {{{time_left}}}', {
                        count:      _count,
                        items:      _count > 1 ? 'images traitées' : 'image traitée',
                        total:      _numFiles,
                        percent:    ( _count / _numFiles * 100 ).toFixed(0),
                        time_left:  toHHMMSS( _timeLeft )
                    });
                    options.$palette.update();
                }
                
            }
            
            if( options.algolia_records ) {
                createFile( _config.outputFolder + 'Images_for_Algolia.json', prettyJSON( _algolia_records ) );
            }
            
            var _finalMsg = _useActiveDocument ? "L'image a été publiée sur le BenoîtBot." : Mustache.render(
                'Export terminé ({{count}} {{items}}).' + 
                '{{#export_png}}\n> Dossier "{{{png_folder}}}"{{/export_png}}' +
                '{{#export_eps}}\n> Dossier "{{{eps_folder}}}"{{/export_eps}}', {
                count:          _numFiles,
                items:          _numFiles > 1 ? 'images' : 'image',
                export_png:     options.export_png,
                export_eps:     options.export_eps,
                png_folder:     Folder.decode( _pngFolder.name ),
                eps_folder:     Folder.decode( _epsFolder.name )
            });
            if( options.$logMsg ) {
                options.$logMsg.text = _finalMsg;
            } else {
                alert( _finalMsg, _config.scriptName );
            }
            
        } catch( e ) {
            alert( "Une erreur est survenue : \n" + logException( e ) );
        }
    }
    

    /**
     * Publish current image
     */
    _self.publishCurrentImage = function() {
        
        var _exportOptions = {
            batch:              false,
            export_png:         true,
            export_eps:         true,
            upload:             true,
            validate_xmp:       true,
            algolia_records:    true
        };
        
        exportImages( _exportOptions );
    };

    /**
     * Build and show the palette
     */
    _self.initUI = function() {
        
        var $paletteUI = buildUI();
        if( $paletteUI === null ) {
            return;
        }

        if( $paletteUI instanceof Window ) {
            $paletteUI.show();
        } else {
            $paletteUI.layout.layout( true );
        }
    };

    return _self;
    
})( BenoitBot.Illustrator || {} );